package fintech.exercises12

import fintech.lecture12.App3.IO

// Реализуйте функцию forever
object TrampoliningExercise {

  /**
    * Создает новое IO, которое бесконечно повторяет исходный action
    */
  def forever[A](action: IO[A]): IO[A] = ???

  def main(args: Array[String]): Unit = {
    forever(IO(() => println("Hello world"))).unsafeRun()
  }
}
